<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Entity
 *
 * @property string $name
 * @property float $price
 * @property User $user
 */
class Product extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected $fillable = [
        'name', 'price', 'user_id'
    ];
}
