<?php


namespace App\Repository;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class AbstractRepository implements RepositoryInterface
{

    public function save(Model $model): Model
    {
        $model->save();
        return $model;
    }

    public function delete(Model $model): void
    {
        $model->delete();
    }

    public function deleteById(int $id): bool
    {
        return $this->getInstanceClass()::destroy($id);
    }

    /**
     * @throws ModelNotFoundException
     */
    public function getById(int $id): Model
    {
        return $this->getInstanceClass()::findOrFail($id);
    }

    public function findAll()
    {
        return $this->getInstanceClass()::all();
    }

    /**
     * @throws \LogicException
     */
    private function getInstanceClass(): string
    {
        if (null === static::INSTANSE_ClASS){
            throw new \LogicException('Repository model instance class is not specified');
        }
        return static::INSTANSE_ClASS;
    }

}
