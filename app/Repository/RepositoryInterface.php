<?php

declare(strict_types=1);

namespace App\Repository;


use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    const INSTANSE_ClASS = null;
    public function save(Model $model): Model;
    public function delete(Model $model): void ;
    public function deleteById(int $id): bool;
    public function getById(int $id): Model ;
    public function findAll();

}
