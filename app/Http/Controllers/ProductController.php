<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{

    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->middleware('auth');
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $this->authorize('viewAny', Product::class);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }

        return view('product/index', [
            'products' => $this->productRepository->findAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $this->authorize('create', Product::class);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }
        return view('product/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->authorize('create', Product::class);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }
        $data = Input::only(['name', 'price']);
        $data['user_id'] = auth()->id();
        Product::create($data);
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->productRepository->getById($id);
        try{
            $this->authorize('view', $product);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }

        return view('product/show', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->getById($id);
        try{
            $this->authorize('update', $product);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }

        return view('product/edit', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = $this->productRepository->getById($id);
        try{
            $this->authorize('update', $product);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }

        $data = Input::only(['name', 'price']);
        $product->update($data);

        return redirect()->route('product.show', ['id' => $product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->getById($id);
        try{
            $this->authorize('delete', $product);
        }catch (AuthorizationException $exception){
            return redirect()->route('product.index');
        }
        $this->productRepository->delete($product);

        return redirect()->route('product.index');
    }
}
