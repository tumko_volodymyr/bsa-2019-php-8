<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/auth/redirect/{provider}', 'Auth\SocialAuthController@redirect');
Route::get('/callback/{provider}', 'Auth\SocialAuthController@callback');

Route::group([
    'prefix' => '/products',
], function () {
    Route::get('/', 'ProductController@index')->name('product.index');
    Route::get('/add', 'ProductController@create')->name('product.add');
    Route::post('/', 'ProductController@store')->name('product.store');
    Route::get('/{id}', 'ProductController@show')->name('product.show');
    Route::get('/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('/{id}', 'ProductController@update')->name('product.update');
    Route::delete('/{id}', 'ProductController@destroy')->name('product.delete');
});
