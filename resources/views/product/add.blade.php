@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-fw fa-plus-circle"></i> <strong>Product</strong>
                    <a href="{{route('product.index')}}" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-list"></i> Products</a>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-6">
                <form method="POST" action="{{ route('product.store') }}" >
                    @csrf
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter name" required>
                    </div>
                    <div class="form-group">
                        <label>Price <span class="text-danger">*</span></label>
                        <input name="price" id="price" class="form-control" placeholder="Enter price" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-plus-circle"></i> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
