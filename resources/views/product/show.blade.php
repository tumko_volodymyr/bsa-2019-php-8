@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-fw fa-gift"></i> <strong>Product</strong>
                        <a href="{{route('product.index')}}" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-list"></i> Products</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        {{$product->name}}
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Price: {{$product->price}}</li>
                    </ul>
                    @canany(['update', 'delete'], $product)
                        <ul class="list-group list-group-flush">
                            @can('update', $product)
                                <li class="list-group-item">
                                    <div align="center">
                                        <a href="{{route('product.edit', $product->id)}}" class="text-primary"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                    </div>
                                </li>
                            @endcan
                            @can('delete', $product)
                                <li class="list-group-item">
                                    <div align="center">
                                        <form method="POST" action="{{route('product.delete', $product->id)}}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" name="submit" value="submit" id="submit"  class="text-danger"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                        </form>
                                    </div>
                                </li>
                            @endcan
                        </ul>
                    @endcanany
                </div>
            </div>
        </div>
    </div>
@endsection
