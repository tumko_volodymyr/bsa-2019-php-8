@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-fw fa-list"></i> <strong>Products</strong>
                    <a href="{{route('product.add')}}" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Add</a>
                </div>
            </div>
        </div>
    </div>
    <tr class="row justify-content-center">
        <table class="table table-striped table-bordered" id="products">
            <thead>
                <tr class="bg-primary text-white">
                    <th>№</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($products as $product)
                <tr>
                    <td>{{$loop->index}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td align="center">
                        <a href="{{route('product.show', $product->id)}}" class="text-success"><i class="fa fa-fw fa-eye"></i> Show</a>
                    </td>
                </tr>
            @empty
                <tr>
                    No products
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
