@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-fw fa-box"></i> <strong>Marketplace</strong>
                    <a href="{{route('product.index')}}" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-list"></i> Products</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
